#ifndef VECTOR2D_H
#define VECTOR2D_H

class Vector2D
{
public:
    Vector2D(int x, int y);
    Vector2D() : x(0), y(0) {}
    int x, y;
};

#endif // VECTOR2D_H
