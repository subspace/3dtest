#ifndef MATHLIB_H
#define MATHLIB_H

#include "math.h"
#include "vector3d.h"
#include "matrix4x4.h"
#include "quaternion.h"
#include "camera.h"
#include <iostream>

// Right handed coordinate
class MathLib
{
public:
    MathLib();

    static float Deg2Rad(float angle) {
        return angle*M_PI/180.0f;
    }

    static float Rad2Deg(float rad) {
        return rad*180.0f/M_PI;
    }

    static float DotProduct(const Vector3D& v1, const Vector3D& v2) {
        return (v1.x*v2.x + v1.y*v2.y + v1.z*v2.z);
    }

    static Vector3D CrossProduct(const Vector3D& v1, const Vector3D& v2) {
        Vector3D cross;
        cross.x = v1.y*v2.z - v1.z*v2.y;
        cross.y = v1.z*v2.x - v1.x*v2.z;
        cross.z = v1.x*v2.y - v1.y*v2.x;
        return cross;
    }

    static Vector3D RotateAroundAxis(const Vector3D& axis, const Vector3D& vector, float angle) {
        Vector3D projVecIntoAxis = MathLib::DotProduct(vector, axis) * axis;
        Vector3D w = MathLib::CrossProduct(axis, vector);
        angle = MathLib::Deg2Rad(angle);
        float cosa = cos(angle);
        float sina = sin(angle);

        return Vector3D(cosa*(vector - projVecIntoAxis) + sina*w + projVecIntoAxis);
    }

    static Matrix4x4 RotationMatrix(const Vector3D& axis, float angle) {
        Matrix4x4 rot;
        Vector3D a(1, 0, 0, 0);
        Vector3D b(0, 1, 0, 0);
        Vector3D c(0, 0, 1, 0);
        Vector3D d(0, 0, 0, 1);
        Vector3D axle = axis.Normalized();
        rot.SetColumn(0, RotateAroundAxis(axle, a, angle));
        rot.SetColumn(1, RotateAroundAxis(axle, b, angle));
        rot.SetColumn(2, RotateAroundAxis(axle, c, angle));
        rot.SetColumn(3, d);
        return rot;
    }

    static Vector3D QuaternionRotation(const Vector3D& axis, const Vector3D& vector, float angle) {
        angle = MathLib::Deg2Rad(angle/2.0f);
        float cosa = cos(angle);
        float sina = sin(angle);
        Quaternion rot(cosa, sina*axis.x, sina*axis.y, sina*axis.z);
        Quaternion invRot(cosa, -rot.x, -rot.y, -rot.z); // Unit quaternion so no need for magnitude AFAIK
        Quaternion vec(0, vector.x, vector.y, vector.z);
        Quaternion result;
        result = rot*vec*invRot;
        return Vector3D(result.x, result.y, result.z);
    }

    static Matrix4x4 GetModelViewMatrix(Camera& cam) {
        Matrix4x4 camRotation;
        Matrix4x4 camTranslation;

        camTranslation.LoadIdentity();
        camTranslation.SetColumn(3, -cam.position);
        camTranslation.m[3][3] = 1;

        camRotation.LoadIdentity();
        camRotation.m[0][0] = cam.right.x;
        camRotation.m[0][1] = cam.right.y;
        camRotation.m[0][2] = cam.right.z;
        camRotation.m[0][3] = 0;

        camRotation.m[1][0] = cam.up.x;
        camRotation.m[1][1] = cam.up.y;
        camRotation.m[1][2] = cam.up.z;
        camRotation.m[1][3] = 0;

        camRotation.m[2][0] = -cam.forward.x; // -Z is the visible portion, the clip space transformation will be done following those
        camRotation.m[2][1] = -cam.forward.y;
        camRotation.m[2][2] = -cam.forward.z;
        camRotation.m[2][3] = 0;


        return camRotation*camTranslation;
    }

    static Matrix4x4 GetPerspectiveMatrix(float fovX, float aspectRatio, float near, float far) {
        float projWidth = near * tan(fovX/2.0f);
        float projHeight = projWidth * aspectRatio;

        // [ n/Pw   0       0       0       ][ x ]   [ -z*clipX ]
        // [ 0      n/Ph    0       0       ][ y ] = [ -z*clipY ]
        // [ 0      0       f+n/n-f 2fn/n-f ][ z ]   [ -z*clipZ ]
        // [ 0      0       -1      0       ][ w ]   [ -z       ]

        Matrix4x4 clipMatrix;
        clipMatrix.LoadIdentity();
        clipMatrix.m[0][0] = near / projWidth;
        clipMatrix.m[1][1] = near / projHeight;
        clipMatrix.m[2][2] = (far+near)/(near-far);
        clipMatrix.m[2][3] = 2*far*near/(near-far);
        clipMatrix.m[3][2] = -1;
        clipMatrix.m[3][3] = 0;

        return clipMatrix;
    }
    
    static Matrix4x4 GetOrthogonalMatrix(float fovX, float aspectRatio, float near, float far) {
        float projWidth = near * tan(fovX/2.0f);
        float projHeight = projWidth * aspectRatio;

        // [ 2/Pw   0       0       0       ][ x ]   [ -z*clipX ]
        // [ 0      2/Ph    0       0       ][ y ] = [ -z*clipY ]
        // [ 0      0       f+n/n-f 2fn/n-f ][ z ]   [ -z*clipZ ]
        // [ 0      0       -1      0       ][ w ]   [ -z       ]

        Matrix4x4 clipMatrix;
        clipMatrix.LoadIdentity();
        clipMatrix.m[0][0] = near / projWidth;
        clipMatrix.m[1][1] = near / projHeight;
        clipMatrix.m[2][2] = (far+near)/(near-far);
        clipMatrix.m[2][3] = 2*far*near/(near-far);
        clipMatrix.m[3][2] = -1;
        clipMatrix.m[3][3] = 0;

        return clipMatrix;
    }

    static Matrix4x4 GetViewportMatrix(int x, int y, int width, int height) {
        // x' = (Xndc+1)*width/2 + x
        // y' = (Yndc+1)*height/2 + y
        Matrix4x4 vpm;
        vpm.LoadIdentity();
        vpm.m[0][0] = width/2.0f;
        vpm.m[1][1] = -height/2.0f;
        vpm.m[2][2] = 0.5f;
        vpm.m[0][3] = width/2.0f + x;
        vpm.m[1][3] = height/2.0f + y;
        vpm.m[2][3] = 0.5f;
        return vpm;
    }

};

#endif // MATHLIB_H
