#ifndef QUAD_H
#define QUAD_H

#include "vector3d.h"

class Quad
{
public:
    Quad(const Vector3D& a, const Vector3D& b, const Vector3D& c, const Vector3D& d);
    Vector3D a, b, c, d;
};

#endif // QUAD_H
