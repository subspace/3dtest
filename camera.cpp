#include "camera.h"
#include "mathlib.h"

Camera::Camera(const Vector3D& position, float yaw, float pitch, float roll):
    position(position), yaw(yaw), pitch(pitch), roll(roll) {
    forward.x = 0;
    forward.y = 0;
    forward.z = 1;
    up.x = 0;
    up.y = 1;
    up.z = 0;
    right.x = 1;
    right.y = 0;
    right.z = 0;
}

void Camera::Update() {
    Matrix4x4 yawRotationMatrix = MathLib::RotationMatrix(Vector3D(0, 1, 0), yaw);
    forward = yawRotationMatrix * Vector3D(0, 0, 1);
    right   = MathLib::CrossProduct(forward, Vector3D(0, 1, 0));

    Matrix4x4 pitchRotationMatrix = MathLib::RotationMatrix(right, pitch);
    forward = pitchRotationMatrix * forward;
    up = MathLib::CrossProduct(right, forward);

    Matrix4x4 rollMatrix = MathLib::RotationMatrix(forward, roll);
    up = rollMatrix * up;
    right = MathLib::CrossProduct(forward, up);

    forward.Normalize();
    right.Normalize();
    up.Normalize();
}

void Camera::TurnLeft(float angle) {
    yaw += angle;
    if (yaw > 360) {
        yaw -= 360;
    } else if (yaw < 0) {
        yaw += 360;
    }
}

void Camera::RollRight(float angle) {
    roll += angle;
    if (roll > 90) {
        roll = 90;
    } else if (roll < -90) {
        roll = -90;
    }
}

void Camera::LookUp(float angle) {
    pitch += angle;
    if (pitch > 90) {
        pitch = 90;
    } else if (pitch < -90) {
        pitch = -90;
    }
}

void Camera::MoveUp(float units) {
    position = position + up*units;
}

void Camera::StrafeRight(float units) {
    position = position + right*units;
}

void Camera::MoveForward(float units) {
    position = position + forward*units;
}
