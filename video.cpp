#include "video.h"
#include "vector2d.h"
#include "triangle.h"
#include <SDL/SDL.h>
#include <iostream>
#include <math.h>

using namespace std;

Uint32 Video::Red, Video::Green, Video::Blue;

Video::Video():
    screen(0) {
}

Video::~Video() {
    Shutdown();
}

bool Video::Init(int w, int h) {
    if (screen)
        return false;

    if (SDL_Init(SDL_INIT_VIDEO) < 0)
        return false;

    screen = SDL_SetVideoMode(w, h, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
    if (!screen)
        return false;

    Red = SDL_MapRGB(screen->format, 255, 0, 0);
    Green = SDL_MapRGB(screen->format, 0, 255, 0);
    Blue = SDL_MapRGB(screen->format, 0, 0, 255);

    return true;
}

void Video::StartDrawing() {
    if (SDL_MUSTLOCK(screen))
        SDL_LockSurface(screen);
}

void Video::EndDrawing() {
    if (SDL_MUSTLOCK(screen))
        SDL_UnlockSurface(screen);
}

void Video::Flip() {
    SDL_Flip(screen);
}

void Video::DrawLine(Vector2D start, Vector2D end, Uint32 color) {
    int deltaX = end.x - start.x;
    int deltaY = end.y - start.y;
    Uint32 pitch = screen->pitch / sizeof(Uint32);

    if (abs(deltaX) > abs(deltaY)) {
        if (start.x > end.x) {
            swap(start, end);
            deltaX = -deltaX;
            deltaY = -deltaY;
        }
        if (!deltaX)
            return;
        float slope = deltaY / float(deltaX);
        Uint32 offset = start.y;
        for (int x = 0; x <= deltaX; ++x) {
            int y = slope*x + offset;
            ((Uint32 *)screen->pixels)[x + y*pitch + start.x] = color;
        }
    } else {
        if (start.y > end.y) {
            swap(start, end);
            deltaX = -deltaX;
            deltaY = -deltaY;
        }
        if (!deltaY)
            return;
        float slope = deltaX / float(deltaY);
        Uint32 offset = start.x;
        for (int y = 0; y <= deltaY; ++y) {
            int x = slope*y + offset;
            ((Uint32 *)screen->pixels)[x + (y + start.y)*pitch] = color;
        }
    }
}

void Video::DrawTriangle(const Triangle &tri, Uint32 color) {
    DrawLine(tri.vertices[0], tri.vertices[1], color);
    DrawLine(tri.vertices[1], tri.vertices[2], color);
    DrawLine(tri.vertices[2], tri.vertices[0], color);
}

void Video::Shutdown() {
    if (screen) {
        SDL_FreeSurface(screen);
        screen = 0;
    }
}

void Video::Clear() {
    SDL_FillRect(screen, NULL, 0x000000);
}
