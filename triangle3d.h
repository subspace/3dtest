#ifndef TRIANGLE3D_H
#define TRIANGLE3D_H

#include "vector3d.h"
#include <iostream>
#include "matrix4x4.h"

using namespace std;

class Triangle3D
{
public:
    Triangle3D() {}
    Triangle3D(const Vector3D& a, const Vector3D& b, const Vector3D& c);
    Vector3D a, b, c;

    void RotateX(float angle);
    void RotateY(float angle);
    void RotateZ(float angle);
};

#endif // TRIANGLE3D_H
