#ifndef QUATERNION_H
#define QUATERNION_H

class Quaternion
{
public:
    float w, x, y, z;
    Quaternion(float w, float x, float y, float z): w(w), x(x), y(y), z(z) {}
    Quaternion() {}

    Quaternion operator*(const Quaternion &other) {
        Quaternion ret;
        ret.w = w*other.w - x*other.x - y*other.y - z*other.z;
        ret.x = w*other.x + x*other.w + y*other.z - z*other.y;
        ret.y = w*other.y + y*other.w + z*other.x - x*other.z;
        ret.z = w*other.z + z*other.w + x*other.y - y*other.x;
        return ret;
    }
};

#endif // QUATERNION_H
