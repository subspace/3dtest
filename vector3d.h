#ifndef VECTOR3D_H
#define VECTOR3D_H

class Vector3D
{
public:
    Vector3D() {}
    Vector3D(float x, float y, float z, float w = 0);

    float Magnitude() const;
    Vector3D Normalized() const;
    void Normalize();
    float Distance(const Vector3D &v1, const Vector3D &v2);

    float x, y, z, w;
};

Vector3D operator +(const Vector3D& v1, const Vector3D& v2);
Vector3D operator -(const Vector3D& v1, const Vector3D& v2);
Vector3D operator -(const Vector3D& v);
Vector3D operator *(float scalar, const Vector3D& v);
Vector3D operator *(const Vector3D& v, float scalar);
Vector3D operator /(const Vector3D& v, float scalar);
Vector3D& operator *=(Vector3D& v, float scalar);
Vector3D& operator /=(Vector3D& v, float scalar);

#endif // VECTOR3D_H
