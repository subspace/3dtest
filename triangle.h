#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "vector2d.h"

class Triangle
{
public:
    Vector2D vertices[3];

    Triangle(const Vector2D& a, const Vector2D& b, const Vector2D& c);

};

#endif // TRIANGLE_H
