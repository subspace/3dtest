#include "common.h"
#include "vector3d.h"
#include "vector2d.h"
#include "matrix4x4.h"
#include "triangle.h"
#include "triangle3d.h"
#include <iostream>

using namespace std;

void print(const Vector3D &vec) {
    cout << "(" << vec.x << ", " << vec.y << ", " << vec.z << ", " << vec.w << ")" << endl;
}

void print(const Vector2D &vec) {
    cout << "(" << vec.x << ", " << vec.y << ")" << endl;
}

void print(const Triangle& tri) {
    print(tri.vertices[0]);
    print(tri.vertices[1]);
    print(tri.vertices[2]);
}

void print(const Triangle3D &tri) {
    print(tri.a);
    print(tri.b);
    print(tri.c);
}
