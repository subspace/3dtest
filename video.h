#ifndef VIDEO_H
#define VIDEO_H

#include <SDL/SDL.h>

class Vector2D;
class Triangle;
class Video
{
public:
    static Uint32 Red, Green, Blue;

    Video();
    ~Video();
    bool Init(int w, int h);
    void Shutdown(void);
    void DrawLine(Vector2D start, Vector2D end, Uint32 color);
    void DrawTriangle(const Triangle& tri, Uint32 color);
    void StartDrawing(void);
    void EndDrawing(void);
    void Flip(void);
    void Clear(void);

    SDL_Surface *screen;
};

#endif // VIDEO_H
