#ifndef COMMON_H
#define COMMON_H

class Vector3D;
class Triangle;
class Triangle3D;
class Vector2D;
class Matrix4x4;

void print(const Vector3D& vec);
void print(const Vector2D& vec);
void print(const Triangle& tri);
void print(const Triangle3D& tri);
void print(const Matrix4x4& mat);

#endif // COMMON_H
