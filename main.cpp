#include <iostream>
#include <unistd.h>
#include <list>
#include "video.h"
#include "vector2d.h"
#include "triangle.h"
#include "triangle3d.h"
#include "matrix4x4.h"
#include "mathlib.h"
#include "camera.h"
#include "common.h"
#include "Q3Loader.h"

using namespace std;

static bool running = true;

// Screen
static const int screenWidth = 800;
static const int screenHeight = 600;

// Mouse settings
const float m_sensitivity = 8.0f;
const float m_pitch = -0.022f;
const float m_yaw = 0.022f;

// SDL
Video v;

// 3D
Camera           camera(Vector3D(0, 0, -300), 0, 0, 0);
Matrix4x4        clipMatrix = MathLib::GetPerspectiveMatrix(90.0f, screenHeight/float(screenWidth), 1, 8192);
Matrix4x4        viewportMatrix = MathLib::GetViewportMatrix(0, 0, screenWidth, screenHeight);
list<Triangle3D> objects;

bool IsVisible(const Triangle3D& tri) {
    if (tri.a.x > 1 || tri.a.x < -1) {
        return false;
    }
    if (tri.a.y > 1 || tri.a.y < -1) {
        return false;
    }
    if (tri.a.z > 1 || tri.a.z < -1) {
        return false;
    }

    if (tri.b.x > 1 || tri.b.x < -1) {
        return false;
    }
    if (tri.b.y > 1 || tri.b.y < -1) {
        return false;
    }
    if (tri.b.z > 1 || tri.b.z < -1) {
        return false;
    }

    if (tri.c.x > 1 || tri.c.x < -1) {
        return false;
    }
    if (tri.c.y > 1 || tri.c.y < -1) {
        return false;
    }
    if (tri.c.z > 1 || tri.c.z < -1) {
        return false;
    }

    return true;
}

void DrawCrosshair() {
    v.DrawLine(Vector2D(screenWidth/2.0f-3, screenHeight/2.0f  ), Vector2D(screenWidth/2.0f+4, screenHeight/2.0f  ), Video::Red);
    v.DrawLine(Vector2D(screenWidth/2.0f-3, screenHeight/2.0f+1), Vector2D(screenWidth/2.0f+4, screenHeight/2.0f+1), Video::Red);

    v.DrawLine(Vector2D(screenWidth/2.0f  , screenHeight/2.0f-3), Vector2D(screenWidth/2.0f  , screenHeight/2.0f+4), Video::Red);
    v.DrawLine(Vector2D(screenWidth/2.0f+1, screenHeight/2.0f-3), Vector2D(screenWidth/2.0f+1, screenHeight/2.0f+4), Video::Red);
}

void Render() {
    camera.Update();
    Matrix4x4 modelView = MathLib::GetModelViewMatrix(camera);

    v.StartDrawing();
    v.Clear();
    list<Triangle3D>::iterator obj = objects.begin();
    while (obj != objects.end()) {
        Triangle3D tri = *obj;
//      cout << "world space" << endl;
//      print(tri);

        // to eye space
        tri.a = modelView*tri.a;
        tri.b = modelView*tri.b;
        tri.c = modelView*tri.c;
//      cout << "eye space" << endl;
//      print(tri);

        // to clip space
        tri.a = clipMatrix*tri.a;
        tri.b = clipMatrix*tri.b;
        tri.c = clipMatrix*tri.c;
//      cout << "clip space" << endl;
//      print(tri);

        // to NDC
        tri.a /= tri.a.w;
        tri.b /= tri.b.w;
        tri.c /= tri.c.w;
//      cout << "ndc" << endl;
//      print(tri);
        if (!IsVisible(tri)) {
            obj++;
            continue;
        }

        // to Screen
        tri.a = viewportMatrix*tri.a;
        tri.b = viewportMatrix*tri.b;
        tri.c = viewportMatrix*tri.c;
//      cout << "screen" << endl;
//      print(tri);

        Triangle triangle2d(Vector2D(tri.a.x, tri.a.y), Vector2D(tri.b.x, tri.b.y), Vector2D(tri.c.x, tri.c.y));
        v.DrawTriangle(triangle2d, Video::Green);
        obj++;
    }
    DrawCrosshair();
    v.EndDrawing();
    v.Flip();
}

void ReadInput() {
    SDL_Event event;
    static int oldx = screenWidth/2, oldy = screenHeight/2;

    while(1) {
        SDL_WaitEvent(&event);
        switch(event.type) {
        case SDL_KEYDOWN:
            switch(event.key.keysym.sym) {
            case SDLK_a:
                camera.StrafeRight(-5);
                return;
            case SDLK_d:
                camera.StrafeRight(5);
                return;
            case SDLK_s:
                camera.MoveForward(-5);
                return;
            case SDLK_w:
                camera.MoveForward(5);
                return;
            case SDLK_q:
                camera.RollRight(-5);
                return;
            case SDLK_e:
                camera.RollRight(5);
                return;
            case SDLK_SPACE:
                camera.MoveUp(5);
                return;
            case SDLK_LALT:
                camera.MoveUp(-5);
                return;
            case SDLK_LEFT:
                camera.TurnLeft(5);
                return;
            case SDLK_RIGHT:
                camera.TurnLeft(-5);
                return;
            case SDLK_UP:
                camera.LookUp(5);
                return;
            case SDLK_DOWN:
                camera.LookUp(-5);
                return;
            case SDLK_ESCAPE:
                running = false;
                return;
            }
            break;
        case SDL_KEYUP:
            break;
        case SDL_MOUSEMOTION:
//            int sx, sy;
//            sx = screenWidth/2;
//            sy = screenHeight/2;

//            float mx, my;
//            int newx, newy;
//            newx = event.motion.x;
//            newy = event.motion.y;
//            if (newx == sx && newy == sy) { // Detect SDL_WarpMouse !!! This is not workin maybe it's accumulating mouse movement !!!
//                return;
//            }

//            mx = newx - sx;
//            my = newy - sy;
//            float mag = sqrt(mx*mx + my*my);
//            mx = -mx / mag * m_yaw * m_sensitivity;
//            my = -my / mag * m_pitch * m_sensitivity;

//            camera.TurnLeft(mx);
//            camera.LookUp(my);

//            SDL_WarpMouse(screenWidth/2, screenHeight/2);

            break;
        }
    }
}

#include <assimp/Importer.hpp>
#include <assimp/mesh.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

void LoadSomeMap() {
    TMapQ3 q3map;

    cout << readMap("./biggun.bsp", q3map) << endl;

    FILE*	lFile = fopen("./final_debug.txt", "w+");
    debugInformations(q3map, lFile);
    fclose(lFile);

    std::vector<TFace>::iterator itr = q3map.mFaces.begin();
    std::vector<TVertex>::iterator vertices = q3map.mVertices.begin();
    cout << q3map.mFaces.size() << endl;
    cout << q3map.mVertices.size() << endl;
}

void LoadNode(const aiScene* scene, const aiNode* node, const aiMatrix4x4& transformation) {
    for (int i = 0; i < node->mNumMeshes; ++i) {
        const aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
        for (int j = 0; j < mesh->mNumFaces; ++j) {
            const aiFace& face = mesh->mFaces[j];
            Triangle3D tri;

            for (int k = 0; k < 3; ++k) {
                aiVector3D pos = transformation * node->mTransformation * mesh->mVertices[face.mIndices[k]];

                if (k == 0) {
                    tri.a.x = pos.x;
                    tri.a.y = pos.y;
                    tri.a.z = pos.z;
                    tri.a.w = 1;
                } else if (k == 1) {
                    tri.b.x = pos.x;
                    tri.b.y = pos.y;
                    tri.b.z = pos.z;
                    tri.b.w = 1;
                } else if (k == 2) {
                    tri.c.x = pos.x;
                    tri.c.y = pos.y;
                    tri.c.z = pos.z;
                    tri.c.w = 1;
                }
            }
            objects.push_back(tri);
        }
    }

    for (int i = 0; i < node->mNumChildren; ++i) {
        LoadNode(scene, node->mChildren[i], transformation * node->mTransformation);
    }
}

#include <assimp/matrix4x4.h>
void LoadSomeModel() {
    Assimp::Importer importer;
    const aiScene *scene = importer.ReadFile("./pistol.3ds", aiProcessPreset_TargetRealtime_Fast);
    const aiNode *root = scene->mRootNode;
    aiMatrix4x4 transform;

    LoadNode(scene, root, root->mTransformation);
}

int main() {
    LoadSomeModel();

    if (!v.Init(screenWidth, screenHeight)) {
        cout << "Failed to initialize SDL." << endl;
        return 1;
    }

    SDL_WarpMouse(screenWidth/2, screenHeight/2);
    SDL_WM_SetCaption("Mihawk's Simple 3d", NULL);
    SDL_ShowCursor(false);
    while(running) {
        Render();
        ReadInput();
    }
    v.Shutdown();

    return 0;
}

