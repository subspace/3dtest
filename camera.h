#ifndef CAMERA_H
#define CAMERA_H

#include "vector3d.h"

// Assuming right handed coords
class Camera
{
public:
    Camera(const Vector3D& position, float yaw, float pitch, float roll);
    void Update();
    void TurnLeft(float angle);
    void LookUp(float angle);
    void RollRight(float angle);
    void MoveForward(float units);
    void StrafeRight(float units);
    void MoveUp(float units);

    float yaw, pitch, roll;
    Vector3D forward, right, up;
    Vector3D position;
};

#endif // CAMERA_H
