#include "vector3d.h"
#include "mathlib.h"

Vector3D::Vector3D(float x, float y, float z, float w):
    x(x), y(y), z(z), w(w)
{
}

float Vector3D::Magnitude( void ) const {
    return float( sqrt( x*x + y*y + z*z ) );
}

Vector3D Vector3D::Normalized( void ) const {
    Vector3D r;
    float mag = Magnitude();
    r.x = x / mag;
    r.y = y / mag;
    r.z = z / mag;
    return r;
}

void Vector3D::Normalize( void ) {
    float mag = Magnitude();
    x /= mag;
    y /= mag;
    z /= mag;
}

float Vector3D::Distance( const Vector3D &v1, const Vector3D &v2 ) {
    float x = v2.x-v1.x;
    float y = v2.y-v1.y;
    float z = v2.z-v1.z;
    return sqrt( x*x + y*y + z*z );
}


Vector3D operator +( const Vector3D& v1, const Vector3D& v2 ) {
    return Vector3D( v1.x+v2.x, v1.y+v2.y, v1.z+v2.z, v1.w+v2.w );
}

Vector3D operator -( const Vector3D& v1, const Vector3D& v2) {
    return Vector3D( v1.x-v2.x, v1.y-v2.y, v1.z-v2.z, v1.w-v2.w );
}

Vector3D operator -( const Vector3D& v ) {
    return Vector3D( -v.x, -v.y, -v.z, -v.w );
}

Vector3D operator *( float scalar, const Vector3D& v ) {
    return Vector3D( v.x*scalar, v.y*scalar, v.z*scalar, v.w*scalar );
}

Vector3D operator *( const Vector3D& v, float scalar ) {
    return Vector3D( v.x*scalar, v.y*scalar, v.z*scalar, v.w*scalar );
}

Vector3D operator /( const Vector3D& v, float scalar ) {
    return Vector3D( v.x/scalar, v.y/scalar, v.z/scalar, v.w/scalar );
}

Vector3D& operator *=( Vector3D& v, float scalar ) {
    v.x = ( v.x*scalar );
    v.y = ( v.y*scalar );
    v.z = ( v.z*scalar );
    v.w = ( v.w*scalar );
    return v;
}

Vector3D& operator /=( Vector3D& v, float scalar ) {
    v.x = ( v.x/scalar );
    v.y = ( v.y/scalar );
    v.z = ( v.z/scalar );
    v.w = ( v.w/scalar );
    return v;
}
