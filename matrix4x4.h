#ifndef MATRIX4X4_H
#define MATRIX4X4_H

#include "vector3d.h"

class Matrix4x4
{
public:
    Matrix4x4();

    float m[4][4];

    void SetRow(int i, const Vector3D& v) {
        m[i][0] = v.x;
        m[i][1] = v.y;
        m[i][2] = v.z;
        m[i][3] = v.w;
    }

    void SetColumn(int j, const Vector3D& v) {
        m[0][j] = v.x;
        m[1][j] = v.y;
        m[2][j] = v.z;
        m[3][j] = v.w;
    }

    Matrix4x4 operator*(const Matrix4x4& other) {
        Matrix4x4 mat;

        // First column
        mat.m[0][0] = m[0][0]*other.m[0][0] + m[0][1]*other.m[1][0] + m[0][2]*other.m[2][0] + m[0][3]*other.m[3][0];
        mat.m[1][0] = m[1][0]*other.m[0][0] + m[1][1]*other.m[1][0] + m[1][2]*other.m[2][0] + m[1][3]*other.m[3][0];
        mat.m[2][0] = m[2][0]*other.m[0][0] + m[2][1]*other.m[1][0] + m[2][2]*other.m[2][0] + m[2][3]*other.m[3][0];
        mat.m[3][0] = m[3][0]*other.m[0][0] + m[3][1]*other.m[1][0] + m[3][2]*other.m[2][0] + m[3][3]*other.m[3][0];

        // Second column
        mat.m[0][1] = m[0][0]*other.m[0][1] + m[0][1]*other.m[1][1] + m[0][2]*other.m[2][1] + m[0][3]*other.m[3][1];
        mat.m[1][1] = m[1][0]*other.m[0][1] + m[1][1]*other.m[1][1] + m[1][2]*other.m[2][1] + m[1][3]*other.m[3][1];
        mat.m[2][1] = m[2][0]*other.m[0][1] + m[2][1]*other.m[1][1] + m[2][2]*other.m[2][1] + m[2][3]*other.m[3][1];
        mat.m[3][1] = m[3][0]*other.m[0][1] + m[3][1]*other.m[1][1] + m[3][2]*other.m[2][1] + m[3][3]*other.m[3][1];

        // Third column
        mat.m[0][2] = m[0][0]*other.m[0][2] + m[0][1]*other.m[1][2] + m[0][2]*other.m[2][2] + m[0][3]*other.m[3][2];
        mat.m[1][2] = m[1][0]*other.m[0][2] + m[1][1]*other.m[1][2] + m[1][2]*other.m[2][2] + m[1][3]*other.m[3][2];
        mat.m[2][2] = m[2][0]*other.m[0][2] + m[2][1]*other.m[1][2] + m[2][2]*other.m[2][2] + m[2][3]*other.m[3][2];
        mat.m[3][2] = m[3][0]*other.m[0][2] + m[3][1]*other.m[1][2] + m[3][2]*other.m[2][2] + m[3][3]*other.m[3][2];

        // Fourth column
        mat.m[0][3] = m[0][0]*other.m[0][3] + m[0][1]*other.m[1][3] + m[0][2]*other.m[2][3] + m[0][3]*other.m[3][3];
        mat.m[1][3] = m[1][0]*other.m[0][3] + m[1][1]*other.m[1][3] + m[1][2]*other.m[2][3] + m[1][3]*other.m[3][3];
        mat.m[2][3] = m[2][0]*other.m[0][3] + m[2][1]*other.m[1][3] + m[2][2]*other.m[2][3] + m[2][3]*other.m[3][3];
        mat.m[3][3] = m[3][0]*other.m[0][3] + m[3][1]*other.m[1][3] + m[3][2]*other.m[2][3] + m[3][3]*other.m[3][3];

        return mat;
    }

    Vector3D operator*(const Vector3D& vec) {
        Vector3D out;
        out.x = vec.x*m[0][0] + vec.y*m[0][1] + vec.z*m[0][2] + vec.w*m[0][3];
        out.y = vec.x*m[1][0] + vec.y*m[1][1] + vec.z*m[1][2] + vec.w*m[1][3];
        out.z = vec.x*m[2][0] + vec.y*m[2][1] + vec.z*m[2][2] + vec.w*m[2][3];
        out.w = vec.x*m[3][0] + vec.y*m[3][1] + vec.z*m[3][2] + vec.w*m[3][3];
        return out;
    }

    void LoadIdentity() {
        m[0][0] = 1;
        m[1][0] = 0;
        m[2][0] = 0;
        m[3][0] = 0;

        m[0][1] = 0;
        m[1][1] = 1;
        m[2][1] = 0;
        m[3][1] = 0;

        m[0][2] = 0;
        m[1][2] = 0;
        m[2][2] = 1;
        m[3][2] = 0;

        m[0][3] = 0;
        m[1][3] = 0;
        m[2][3] = 0;
        m[3][3] = 1;
    }
};

#endif // MATRIX4X4_H
